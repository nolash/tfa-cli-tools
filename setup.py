from setuptools import setup

setup(
        name='tfa-cli-tools',
        version='0.0.2',
        description='CLI commands for common TFA-related operations',
        author='Louis Holbrook',
        author_email='dev@holbrook.no',
        install_requires=[
            'pyotp==2.3.0',
            ],
        scripts=[
            'scripts/otp',
            ],
        url='https://gitlab.com/nolash/tfa-cli-tools',
        classifiers=[
            'Programming Language :: Python :: 3',
            'Operating System :: OS Independent',
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: End Users/Desktop',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Topic :: Utilities',
            ],
        )
